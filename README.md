# nako_dd2ship

## Aim
Create metadata for NAKO

## Files

The file __convert_csvNAKO_2_dataquieR.R__ contains the code that STS provided 
and that he used to convert the csv files from NAKO to a dataquieR format.

The file __import_NAKO_csv_files.R__ contains the adjusted code that ES provided starting from the previous file __convert_csvNAKO_2_dataquieR.R__. It has been used to produce the first metadata file called 	NAKO_dataquieR_compatible_metadata_v01.xlsx


